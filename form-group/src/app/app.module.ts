import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FoneMaskPipe } from './pipes/fone-mask/fone-mask.pipe';

import { AppComponent } from './app.component';
import { FormComponent } from './components/form/form.component';
import { HttpClientModule } from '@angular/common/http';
import { ErrorMsgComponent } from './components/error-msg/error-msg.component';
import { DateMaskPipe } from './pipes/date-mask/date-mask.pipe';
import { CpfMaskPipe } from './pipes/cpf-mask/cpf-mask.pipe';
import { CepMaskPipe } from './pipes/cep-mask/cep-mask.pipe';
import { FormService } from './services/form-service.service';
import { EstadoMaskPipe } from './pipes/estado-mask/estado-mask.pipe';

@NgModule({
  declarations: [
    FoneMaskPipe,
    AppComponent,
    FormComponent,
    ErrorMsgComponent,
    DateMaskPipe,
    CpfMaskPipe,
    CepMaskPipe,
    EstadoMaskPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
