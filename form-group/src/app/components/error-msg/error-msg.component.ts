import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-error-msg',
  templateUrl: './error-msg.component.html',
  styleUrls: ['./error-msg.component.scss']
})
export class ErrorMsgComponent implements OnInit {
  @Input() control: string;
  @Input() errorMsg: string[];
  @Input() validators: string[];
  @Input() form: FormGroup;
  public formLocal: FormGroup;
  public showError = false;
  public msg = '';
  constructor() { }
  ngOnInit() {
  }
}
