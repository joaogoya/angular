import { CustomValidators } from '../../customValidators';
import { HttpClientModule } from '@angular/common/http';
import { Component, OnInit, ɵConsole } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidationErrors,
} from '@angular/forms';
import { FormService } from 'src/app/services/form-service.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  public form: FormGroup;
  public requireddMsg = 'Este campo é obrigatório.';
  public onlyAlphaMsg = 'Apenas letras.';
  public onlyNumbrsMsg = 'Apenas números';
  public errrosList = [];

  constructor(
    private formBuilder: FormBuilder,
    private formService: FormService
  ) {}

  ngOnInit() {
    this.setFormBuilder();
  }

  private setFormBuilder() {
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          CustomValidators.onlyAlpha,
          CustomValidators.minLength.bind(this, 3),
        ],
      ],
      lastName: [
        null,
        [
          Validators.required,
          CustomValidators.onlyAlpha,
          CustomValidators.minLength.bind(this, 3),
        ],
      ],
      fone: [
        null,
        [
          Validators.required,
          CustomValidators.maxLength.bind(this, 16),
          CustomValidators.minLength.bind(this, 16),
        ],
      ],
      email: [
        null,
        [
          Validators.required,
          CustomValidators.onlyAlpha,
          CustomValidators.minLength.bind(this, 7),
          CustomValidators.mailFormat,
        ],
      ],
      birthDay: [null, [Validators.required, CustomValidators.dateMaxValue]],
      cpf: [
        null,
        [
          Validators.required,
          CustomValidators.onlyNumbers,
          CustomValidators.minLength.bind(this, 14),
          CustomValidators.maxLength.bind(this, 14),
        ],
      ],
      cep: [
        null,
        [
          Validators.required,
          CustomValidators.onlyNumbers,
          CustomValidators.minLength.bind(this, 11),
          CustomValidators.maxLength.bind(this, 11),
        ],
        CustomValidators.cepValidator.bind(this, this.formService),
      ],
      lorgadouro: [
        null,
        [
          Validators.required,
          CustomValidators.onlyAlpha,
          CustomValidators.minLength.bind(this, 3),
        ],
      ],
      numero: [null, [Validators.required, CustomValidators.onlyNumbers]],
      complemento: [null, [Validators.required, CustomValidators.onlyNumbers]],
      bairro: [
        null,
        [
          Validators.required,
          CustomValidators.onlyAlpha,
          CustomValidators.minLength.bind(this, 3),
        ],
      ],
      cidade: [
        null,
        [
          Validators.required,
          CustomValidators.minLength.bind(this, 3),
          CustomValidators.onlyAlpha,
        ],
      ],
      estado: [
        null,
        [
          Validators.required,
          CustomValidators.minLength.bind(this, 2),
          CustomValidators.maxLength.bind(this, 2),
          CustomValidators.onlyAlpha,
        ],
      ],
    });
  }

  public setAddress() {
    const res = JSON.parse(localStorage.getItem('cepData'));
    localStorage.removeItem('cepData');
    if (res) {
      this.form.get('lorgadouro').setValue(res.logradouro);
      this.form.get('bairro').setValue(res.bairro);
      this.form.get('cidade').setValue(res.localidade);
      this.form.get('estado').setValue(res.uf);
    }

  }

  public applyCssFeedback(input) {
    if (this.form.get(input).touched) {
      return {
        'is-invalid': !this.testsValidField(input, this.form),
        'is-valid': this.testsValidField(input, this.form),
        'has-feedback': this.form.get(input).touched,
      };
    }
  }

  public testsValidField(input, form) {
    return form.get(input).valid && form.get(input).touched;
  }

  public testFormValid(formGrup: FormGroup) {
    Object.keys(formGrup.controls).forEach((campo) => {
      const controle = formGrup.get(campo);
      controle.markAsTouched();
      if (controle instanceof FormGroup) {
        this.testFormValid(controle);
      }
    });
  }

  public getGrrorList() {
    Object.keys(this.form.controls).forEach((key) => {
      const controlErrors: ValidationErrors = this.form.get(key).errors;
      if (controlErrors) {
        this.errrosList.push({control: key});
      }
    });
  }

  public removeDups(errors) {
    const unique = {};
    errors.forEach((i) => {
      if (!unique[i]) {
        unique[i] = true;
      }
    });
    return Object.keys(unique);
  }

  public onSubmit() {
    this.testFormValid(this.form);
    this.getGrrorList();
  }
}
