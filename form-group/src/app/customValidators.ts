import { FormService } from './services/form-service.service';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { map} from 'rxjs/operators';

export class CustomValidators {
  constructor() {}

  static maxLength(
    limit: number,
    control: AbstractControl
  ): ValidationErrors | null {
    if (control.value) {
      if (control.value.length > limit) {
        return { maxLength: true };
      }
      return null;
    }
    return null;
  }

  static minLength(
    limit: number,
    control: AbstractControl
  ): ValidationErrors | null {
    if (control.value) {
      if (control.value.length < limit) {
        return { minLength: true };
      }
      return null;
    }
    return null;
  }

  static onlyAlpha(control: AbstractControl): ValidationErrors | null {
    const regex = /^([^0-9]*)$/;
    if (!regex.test(control.value)) {
      return { onlyAlpha: true };
    }
    return null;
  }

  static onlyNumbers(control: AbstractControl): ValidationErrors | null {
    const regex = /[a-zA-Z]/;
    if (regex.test(control.value)) {
      return { onlyNumbers: true };
    }
    return null;
  }

  /* mail */
  static mailFormat(control: AbstractControl): ValidationErrors | null {
    if (control.value) {
      const atSign = control.value.includes('@');
      const endPoint = control.value.includes('.');
      if (!(atSign && endPoint)) {
        return { mailFormat: true };
      }
      return null;
    }
    return null;
  }

  static dateMaxValue(control: AbstractControl): ValidationErrors | null {
    if (control.value) {
      const day = control.value.split('/')[0];
      const month = control.value.split('/')[1];
      const year = control.value.split('/')[2];
      const date = year + '-' + month + '-' + day;
      const birthday = new Date(date);
      const regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
      const today = new Date();
      if (birthday > today || !regex.test(date)) {
        return { dateMaxValue: true };
      } else {
        return null;
      }
    }
    return null;
  }

  static cepValidator(formService: FormService, control: AbstractControl): ValidationErrors | null {
    const cep = control.value.replace(/\D/g, '');
    return formService.buscaCep(cep).pipe(
      map(res => {
        if ( res.cep ) {
          localStorage.setItem('cepData', JSON.stringify(res));
          document.getElementById('lorgadouro').focus();
          return null;
        } else {
          document.getElementById('lorgadouro').focus();
          return { cepValidator: true} ;
        }
      })
    );
  }
}
