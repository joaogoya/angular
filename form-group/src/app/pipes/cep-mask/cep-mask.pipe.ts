import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cepMask'
})
export class CepMaskPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      let cpf = value;
      if (value.length === 2 ) {
        cpf = value + value.slice((value.length - 1), 1) + '.';
      }
      if (value.length === 6) {
        cpf = value + value.slice(7, 8) + '.';
      }
      if (value.length === 8) {
        cpf = value + value.slice(8, 9) + '-';
      }
      return cpf;
    }
    return null;
  }

}
