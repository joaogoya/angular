import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cpfMask'
})
export class CpfMaskPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    // 002.512.210-80
    if (value) {
      let cpf = value;
      if (value.length === 3) {
        cpf = value + value.slice((value.length - 1), 2) + '.';
      }
      if (value.length === 7) {
        cpf = value + value.slice(8, 9) + '.';
      }
      if (value.length === 11) {
        cpf = value + value.slice(12, 13) + '-';
      }
      return cpf;
    }
    return null;
  }
}

