import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateMask'
})
export class DateMaskPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      let date = value;
      if (value.length === 2) {
        date = date + '/';
        console.log(date);
      }
      if (value.length === 5) {
        date = date + '/';
        console.log(date);
      }
      return date;
    }
    return null;
  }

}
