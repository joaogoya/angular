import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estadoMask'
})
export class EstadoMaskPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      return value.toUpperCase();
    }
    return null;
  }

}
