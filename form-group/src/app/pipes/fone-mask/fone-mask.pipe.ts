import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'foneMask'
})
export class FoneMaskPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      let fone = value;
      if (value.length === 2 && !value.includes('(') ) {
        fone =
          value.slice((value.length - 1), 0)
          + '(' + value.slice(0, 2) + ') ';
      }
      if (value.length === 6) {
        fone = value + value.slice(6, 7) + ' ';
      }
      if (value.length === 11) {
        fone = value + value.slice(11, value.length) + ' ';
      }
      return fone;
    }
    return null;
  }
}
