import { AbstractControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private readonly cepUrl = 'https://viacep.com.br/ws/';

  constructor(private http: HttpClient) { }

  public buscaCep(cep: any) {
    return this.http.get<any>(this.cepUrl + cep + '/json')
  }
}
